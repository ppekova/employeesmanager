Preface

EmployeesManager is simple management application with Search and Comments features included.

EmployeesManager uses CQRS architectural pattern.

EmployeesManager was developed using:

	- .Net Core 3.1, 
	- PostgreSQL 13, 
	- Dapper 2.0.35
	- DbUP 4.4.0,
	- MediatR 9
	- AutoMapper 10.
	

Application Details

EmployeesManager solution contains 4 projects:

	- Domain 
	Here are located EmployeesManager entities.
	
	- Infrastructure 
	Contains DatabaseAdapterService which provides database connectivity for storing and querying data.
	Contains DatabaseMigrationService which provides database creation, database migrations and data seed on EmployeesManager's start.
	Note: Database connection string is composed using environment variables, described in launchSettings.json.
	
	
	- Application
	Contains MediatR commands and queries for storing and querying data.
	Contains view models classes.
	
	- WebUI
	Represents the presentation layer of EmployeesManager.
	Contains MVC controllers and razor Views.