﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class Job
    {
        public int ID { get; set; }
        public string Title { get; set; }
    }
}
