﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class Comment
    {
        public int ID { get; set; }
        public DateTime DateCreated { get; set; }
        public string Author { get; set; }
        public string Content { get; set; }
        public int EmployeeID { get; set; }
    }
}
