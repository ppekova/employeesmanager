﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class Employee
    {
        public int ID { get; set; }
        public DateTime JoinedCompanyDate { get; set; }
        public string Name { get; set; }
        public string Family { get; set; }
        public string Address { get; set; }
        public SalaryAmountTypes SalaryAmountType { get; set; }
        public decimal SalaryAmount { get; set; }
        public int JobID { get; set; }
        public int DepartmentID { get; set; }
        public int? LineManagerID { get; set; }

        public Department Department { get; set; }
        public Job Job { get; set; }
        public Employee LineManager { get; set; }

       public IEnumerable<Comment> Comments { get; set; }
    }
}
