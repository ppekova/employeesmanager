﻿using Application.Common.Interfaces;
using Application.Employees.Models;
using Dapper;
using Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Employees.Queries
{
    public class GetEmployeesQuery : IRequest<IEnumerable<EmployeeViewModel>>
    {
        public GetEmployeesQuery()
        {
            SearchFilterViewModel = new SearchFilterViewModel();
        }

        public SearchFilterViewModel SearchFilterViewModel { get; set; }
        public class GetEmployeesQueyHandler : IRequestHandler<GetEmployeesQuery, IEnumerable<EmployeeViewModel>>
        {
            IDatabaseAdapterService databaseAdapterService;

            public GetEmployeesQueyHandler(IDatabaseAdapterService databaseAdapterService)
            {
                this.databaseAdapterService = databaseAdapterService;
            }

            public async Task<IEnumerable<EmployeeViewModel>> Handle(GetEmployeesQuery request, CancellationToken cancellationToken)
            {
                var builder = new SqlBuilder();
                var parameters = new DynamicParameters();
                var searchFilter = request.SearchFilterViewModel;

                var selector = builder.AddTemplate(@"SELECT employees.id, employees.name, employees.family, employees.address, 
                    employees.salary_amount_type, employees.salary_amount, employees.joined_company_date,
                    jobs.title as job_title, departments.name as department_name, 
	                (linemanagers.name || ' ' || linemanagers.family) as line_manager_name
                    FROM employees
                    INNER JOIN jobs on jobs.id = employees.job_id
                    INNER JOIN departments on departments.id = employees.department_id
                    LEFT JOIN employees as linemanagers on employees.line_manager_id = linemanagers.ID /**where**/");

                if (searchFilter.DepartmentID > 0)
                {
                    builder.Where("public.departments.id = @DepartmentID");
                    parameters.Add("@DepartmentID", searchFilter.DepartmentID);
                }

                if (searchFilter.JobID > 0)
                {
                    builder.Where("public.employees.job_id = @JobID");
                    parameters.Add("@JobID", searchFilter.JobID);
                }

                if (!string.IsNullOrEmpty(searchFilter.Name))
                {
                    builder.Where("public.employees.name = @Name");
                    parameters.Add("@Name", searchFilter.Name);
                }

                if (!string.IsNullOrEmpty(searchFilter.Family))
                {
                    builder.Where("public.employees.family = @Family");
                    parameters.Add("@Family", searchFilter.Family);
                }

                var data = await this.databaseAdapterService.Query<EmployeeViewModel>(selector.RawSql, parameters);

                return data;
            }
        }

    }
}
