﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Employees.Models
{
    public class SearchFilterViewModel
    {
        public string Name { get; set; }
        public string Family { get; set; }
        public int JobID { get; set; }
        public int DepartmentID { get; set; }
    }
}
