﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Employees.Models
{
    public class EmployeeViewModel
    {
        public int ID { get; set; }

        [Display(Name = "Name")]
        [Required]
        [StringLength(maximumLength: 20, ErrorMessage = "The Name lengths should be 2 and 20", MinimumLength = 2)]
        public string Name { get; set; }

        [Display(Name = "Family")]
        [Required]
        [StringLength(maximumLength: 20, ErrorMessage = "The Name lengths should be 2 and 20", MinimumLength = 2)]
        public string Family { get; set; }

        [Display(Name = "Salary Amount")]
        [Required]
        [DataType(DataType.Currency)]
        public decimal SalaryAmount { get; set; }

        [Display(Name = "Salary Type")]
        public int SalaryAmountType { get; set; }

        [Display(Name = "Salary Type")]
        public string SalaryAmountTypeText
        {
            get { return Enum.GetName(typeof(SalaryAmountTypes), SalaryAmountType); }
        }

        [Display(Name = "Joined Company")]
        [Required]
        [DataType(DataType.Date)]
        public DateTime JoinedCompanyDate { get; set; }

        [Display(Name = "Address")]
        [StringLength(maximumLength: 200, ErrorMessage = "The Name lengths should be 2 and 200", MinimumLength = 2)]
        public string Address { get; set; }

        [Display(Name = "Job Title")]
        public string JobTitle { get; set; }

        [Display(Name = "Department Name")]
        public string DepartmentName { get; set; }
        
        [Display(Name = "Line Manager Name")]
        public string LineManagerName { get; set; }
    }
}
