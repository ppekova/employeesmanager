﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Queries.Departments
{
    public class DepartmentViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
