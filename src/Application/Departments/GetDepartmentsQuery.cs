﻿using Application.Common.Interfaces;
using AutoMapper;
using Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Queries.Departments
{
    public class GetDepartmentsQuery : IRequest<IEnumerable<DepartmentViewModel>>
    {
        public class GetJobsQueryHandler : IRequestHandler<GetDepartmentsQuery, IEnumerable<DepartmentViewModel>>
        {
            IDatabaseAdapterService databaseAdapterService;
            IMapper mapper;

            public GetJobsQueryHandler(IDatabaseAdapterService databaseAdapterService, IMapper mapper)
            {
                this.databaseAdapterService = databaseAdapterService;
                this.mapper = mapper;
            }

            public async Task<IEnumerable<DepartmentViewModel>> Handle(GetDepartmentsQuery request, CancellationToken cancellationToken)
            {
                string sql = @"SELECT id, name FROM public.departments;";
                var data = (await databaseAdapterService.Query<Department>(sql)).ToList();
                data.Add(new Department() { ID = 0, Name = "" });
                return this.mapper.Map<IEnumerable<DepartmentViewModel>>(data.OrderBy(d=>d.ID));
            }
        }
    }
}
