using AutoMapper;

namespace Solarity.Web.Service.Application.Common.Mappings
{
    public interface IMappingConfiguration
    {
        void Mapping(Profile profile);
    }
}