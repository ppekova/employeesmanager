using Application.Comments.Models;
using Application.Queries.Departments;
using Application.Queries.Jobs;
using AutoMapper;
using Domain.Entities;

namespace Solarity.Web.Service.Application.Common.Mappings
{
    public class EmployeesExplorerProfile : Profile
    {
        public EmployeesExplorerProfile()
        {
            CreateMap<Job, JobViewModel>();
            CreateMap<Department, DepartmentViewModel>();
            CreateMap<Comment, CommentViewModel>();
            CreateMap<CommentViewModel, Comment>();
        }
    }
}