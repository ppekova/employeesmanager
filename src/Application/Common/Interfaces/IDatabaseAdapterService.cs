﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Common.Interfaces
{
    public interface IDatabaseAdapterService
    {
        Task<IEnumerable<T>> Query<T>(string query, DynamicParameters parameters = null);
        Task<T> ExecuteScalar<T>(string query, DynamicParameters parameters);
        Task<T> QuerySingle<T>(string query, DynamicParameters parameters);
    }
}
