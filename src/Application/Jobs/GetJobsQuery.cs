﻿using Application.Common.Interfaces;
using AutoMapper;
using Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Queries.Jobs
{
    public class GetJobsQuery : IRequest<IEnumerable<JobViewModel>>
    {
        public class GetJobsQueryHandler : IRequestHandler<GetJobsQuery, IEnumerable<JobViewModel>>
        {
            IDatabaseAdapterService databaseAdapterService;
            IMapper mapper;

            public GetJobsQueryHandler(IDatabaseAdapterService databaseAdapterService, IMapper mapper)
            {
                this.databaseAdapterService = databaseAdapterService;
                this.mapper = mapper;
            }

            public async Task<IEnumerable<JobViewModel>> Handle(GetJobsQuery request, CancellationToken cancellationToken)
            {
                string sql = @"SELECT id, title	FROM public.jobs;";
                var data = (await databaseAdapterService.Query<Job>(sql)).ToList();
                data.Add(new Job() { ID = 0, Title = "" });
                return this.mapper.Map<IEnumerable<JobViewModel>>(data.OrderBy(d => d.ID));
            }
        }
    }
}
