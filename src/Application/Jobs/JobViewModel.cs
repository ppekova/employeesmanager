﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Queries.Jobs
{
    public class JobViewModel
    {
        public int ID { get; set; }
        public string Title { get; set; }
    }
}
