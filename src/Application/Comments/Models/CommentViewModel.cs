﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Comments.Models
{
    public class CommentViewModel
    {
        [DataType(DataType.Date)]
        [ReadOnly(true)]
        [Display(Name = "Created")]
        public DateTime DateCreated { get { return DateTime.Now; } }

        [Required]
        [StringLength(100)]
        public string Author { get; set; }

        [Required]
        [StringLength(500)]
        public string Content { get; set; }

        [Required]
        public int EmployeeID { get; set; }

        [Required]
        public int ID { get; set; }
    }
}
