﻿using Application.Comments.Models;
using Application.Common.Interfaces;
using AutoMapper;
using Dapper;
using Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Comments.Queries
{
    public class GetCommentsQuery : IRequest<IEnumerable<CommentViewModel>>
    {
        public int EmployeeID { get; set; }

        public class GetCommentsQueryHandler : IRequestHandler<GetCommentsQuery, IEnumerable<CommentViewModel>>
        {
            IDatabaseAdapterService databaseAdapterService;
            IMapper mapper;

            public GetCommentsQueryHandler(IDatabaseAdapterService databaseAdapterService, IMapper mapper)
            {
                this.databaseAdapterService = databaseAdapterService;
                this.mapper = mapper;
            }

            public async Task<IEnumerable<CommentViewModel>> Handle(GetCommentsQuery request, CancellationToken cancellationToken)
            {
                string query = @"SELECT id, date_created, author, content, employee_id
                    FROM public.comments
                    WHERE employee_id = @EmployeeID";

                var parameters = new DynamicParameters();
                parameters.Add("@EmployeeID", request.EmployeeID);

                var data = await this.databaseAdapterService.Query<Comment>(query, parameters);
                return this.mapper.Map<IEnumerable<CommentViewModel>>(data);
            }
        }
    }
}
