﻿using Application.Comments.Commands;
using Application.Comments.Models;
using Application.Common.Interfaces;
using AutoMapper;
using Dapper;
using Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Comments.Queries
{
    public class GetCommentQuery : IRequest<CommentViewModel>
    {
        public int CommentID { get; set; }

        public class GetCommentQueryHandler : IRequestHandler<GetCommentQuery, CommentViewModel>
        {
            IDatabaseAdapterService databaseAdapterService;
            IMapper mapper;

            public GetCommentQueryHandler(IDatabaseAdapterService databaseAdapterService, IMapper mapper)
            {
                this.databaseAdapterService = databaseAdapterService;
                this.mapper = mapper;
            }

            public async Task<CommentViewModel> Handle(GetCommentQuery request, CancellationToken cancellationToken)
            {
                string query = @"SELECT id, date_created, author, content, employee_id
                    FROM public.comments
                    WHERE id = @ID";

                var parameters = new DynamicParameters();
                parameters.Add("@ID", request.CommentID);

                var data = await this.databaseAdapterService.QuerySingle<Comment>(query, parameters);

                return this.mapper.Map<CommentViewModel>(data);
            }
        }
    }
}
