﻿using Application.Comments.Models;
using Application.Common.Interfaces;
using AutoMapper;
using Dapper;
using Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Comments.Commands
{
    public class CreateCommentCommand : IRequest<int>
    {
        public CommentViewModel CreateCommentViewModel { get; set; }

        public class CreateCommentCommandHandler : IRequestHandler<CreateCommentCommand, int>
        {
            IDatabaseAdapterService databaseAdapterService;
            IMapper mapper;

            public CreateCommentCommandHandler(IDatabaseAdapterService databaseAdapterService, IMapper mapper)
            {
                this.databaseAdapterService = databaseAdapterService;
                this.mapper = mapper;
            }

            public async Task<int> Handle(CreateCommentCommand request, CancellationToken cancellationToken)
            {
                string sqlQuery = @"INSERT INTO comments(date_created, author, content, employee_id)
	                VALUES (@DateCreated, @Author, @Content, @EmployeeId);
                    SELECT currval('comments_id_seq')";

                var commentDTO = this.mapper.Map<Comment>(request.CreateCommentViewModel);

                var parameters = new DynamicParameters();
                parameters.Add("@DateCreated", commentDTO.DateCreated);
                parameters.Add("@Author", commentDTO.Author);
                parameters.Add("@Content", commentDTO.Content);
                parameters.Add("@EmployeeId", commentDTO.EmployeeID);

                return await this.databaseAdapterService.ExecuteScalar<int>(sqlQuery, parameters);
            }
        }
    }
}
