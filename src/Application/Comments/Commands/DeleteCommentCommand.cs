﻿using Application.Common.Interfaces;
using Dapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Comments.Commands
{
    public class DeleteCommentCommand : IRequest<int>
    {
        public int CommentID { get; set; }

        public class DeleteCommentCommandHandler : IRequestHandler<DeleteCommentCommand, int>
        {
            private IDatabaseAdapterService databaseAdapterService;

            public DeleteCommentCommandHandler(IDatabaseAdapterService databaseAdapterService)
            {
                this.databaseAdapterService = databaseAdapterService;
            }
            
            public async Task<int> Handle(DeleteCommentCommand request, CancellationToken cancellationToken)
            {
                string sqlQuery = @"DELETE FROM public.comments
	                                WHERE id = @ID;";

                var parameters = new DynamicParameters();
                parameters.Add("@ID", request.CommentID);

                return await this.databaseAdapterService.ExecuteScalar<int>(sqlQuery, parameters);
            }
        }
    }
}
