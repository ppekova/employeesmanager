﻿using Application.Comments.Models;
using Application.Common.Interfaces;
using AutoMapper;
using Dapper;
using Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Comments.Commands
{
    public class EditCommentCommand : IRequest<int>
    {
        public CommentViewModel EditCommentViewModel { get; set; }

        public class UpdateCommentCommandHandler : IRequestHandler<EditCommentCommand, int>
        {
            IDatabaseAdapterService databaseAdapterService;
            IMapper mapper;

            public UpdateCommentCommandHandler(IDatabaseAdapterService databaseAdapterService, IMapper mapper)
            {
                this.databaseAdapterService = databaseAdapterService;
                this.mapper = mapper;
            }

            public async Task<int> Handle(EditCommentCommand request, CancellationToken cancellationToken)
            {
                string sqlQuery = @"UPDATE public.comments
	                                SET content=@Content
	                                WHERE id = @ID;";

                var commentDTO = this.mapper.Map<Comment>(request.EditCommentViewModel);

                var parameters = new DynamicParameters();
                parameters.Add("@Content", commentDTO.Content);
                parameters.Add("@ID", commentDTO.ID);

                return await this.databaseAdapterService.ExecuteScalar<int>(sqlQuery, parameters);
            }
        }
    }
}
