﻿using System;
using Application.Common.Interfaces;
using Infrastructure.Persistence;
using Infrastructure.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services)
        {
            services.Configure<DatabaseOptions>(options =>
            {
                options.DBHost = Environment.GetEnvironmentVariable("POSTGRES_HOST_CONNSTR");
                options.DBName = Environment.GetEnvironmentVariable("POSTGRES_DB");
            });

            services.AddTransient<IDatabaseMigrationService, DatabaseMigrationService>();
            services.AddTransient<IDatabaseAdapterService, DatabaseAdapterService>();

            return services;
        }
    }
}
