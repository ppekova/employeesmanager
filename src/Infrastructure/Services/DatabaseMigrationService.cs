﻿using Application.Common.Interfaces;
using DbUp;
using Infrastructure.Persistence;
using Microsoft.Extensions.Options;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Services
{
    public class DatabaseMigrationService : IDatabaseMigrationService
    {
        public DatabaseMigrationService(IOptions<DatabaseOptions> options)
        {
            this.dbOptions = options;
        }

        private IOptions<DatabaseOptions> dbOptions;

        public async Task<bool> GetContextAndMigrate()
        {
            // Create database
            await using (var connection = new NpgsqlConnection(this.dbOptions.Value.DBHost))
            {
                connection.Open();

                await using var cmd = connection.CreateCommand();
                cmd.CommandText = $"SELECT 1 FROM pg_database WHERE datname='{this.dbOptions.Value.DBName}'";
                var dbExists = cmd.ExecuteScalar() != null;

                if (!dbExists)
                {
                    cmd.CommandText = $"create database \"{this.dbOptions.Value.DBName}\";";
                    cmd.ExecuteNonQuery();
                }
            }

            var updater = DeployChanges.To
                .PostgresqlDatabase(this.dbOptions.Value.ConnectionString)
                .WithScriptsEmbeddedInAssembly(Assembly.GetAssembly(typeof(DatabaseMigrationService)))
                .WithTransaction()
                .Build();

            var result = updater.PerformUpgrade();

            if (result.Successful)
                return true;

            throw result.Error;
        }
    }
}
