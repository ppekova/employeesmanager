﻿using Application.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Application;
using Dapper;
using System.Threading.Tasks;
using Npgsql;
using Microsoft.Extensions.Options;
using Infrastructure.Persistence;

namespace Infrastructure.Services
{
    public class DatabaseAdapterService : IDatabaseAdapterService
    {
        private readonly IOptions<DatabaseOptions> dbOptions;

        public DatabaseAdapterService(IOptions<DatabaseOptions> options)
        {
            this.dbOptions = options;
        }

        public async Task<T> QuerySingle<T>(string query, DynamicParameters parameters)
        {
            try
            {
                Dapper.DefaultTypeMap.MatchNamesWithUnderscores = true;
                using var connection = new NpgsqlConnection(dbOptions.Value.ConnectionString);
                connection.Open();
                return await connection.QuerySingleAsync<T>(query, parameters);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public async Task<T> ExecuteScalar<T>(string query, DynamicParameters parameters)
        {
            try
            {
                using var connection = new NpgsqlConnection(dbOptions.Value.ConnectionString);
                connection.Open();
                return await connection.ExecuteScalarAsync<T>(query, parameters);
            }
            catch(Exception ex)
            {
                throw (ex);
            }
        }

        public async Task<IEnumerable<T>> Query<T>(string query, DynamicParameters parameters = null)
        {
            try
            {
                Dapper.DefaultTypeMap.MatchNamesWithUnderscores = true;
                using var connection = new NpgsqlConnection(dbOptions.Value.ConnectionString);
                connection.Open();
                return await connection.QueryAsync<T>(query, parameters);
            }
            catch(Exception ex)
            {
                throw (ex);
            }
        }
    }
}
