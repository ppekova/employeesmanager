﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Infrastructure.Extensions
{
    public static class Extensions
    {
        public static IHost MigrateDatabase(this IHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var databaseMigrationService = scope.ServiceProvider.GetRequiredService<IDatabaseMigrationService>();
                Task.WaitAll(databaseMigrationService.GetContextAndMigrate());
            }
            return host;
        }
    }
}
