﻿using Application.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;

namespace Infrastructure.Persistence
{
    public class DatabaseOptions
    {
        public string DBHost { get; set; }
        public string DBName { get; set; }

        public string ConnectionString { get => $"{DBHost}database={DBName}"; }
    }
}
