-- Departments creation
CREATE TABLE public.departments
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    name character varying(50) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT pk_department PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.departments
    OWNER to postgres;


-- Jobs creation
CREATE TABLE public.jobs
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    title character varying(50) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT pk_jobs PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.jobs
    OWNER to postgres;

-- Employees creation
CREATE SEQUENCE public.employees_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.employees_id_seq
    OWNER TO postgres;


CREATE TABLE public.employees
(
    id integer NOT NULL DEFAULT nextval('employees_id_seq'::regclass),
    name character varying(50) COLLATE pg_catalog."default" NOT NULL,
    family character varying COLLATE pg_catalog."default" NOT NULL,
    address character varying(250) COLLATE pg_catalog."default",
    salary_amount_type smallint NOT NULL,
    salary_amount numeric(18,2) NOT NULL,
    job_id integer NOT NULL,
    department_id integer NOT NULL,
    line_manager_id integer NULL,
    joined_company_date date NOT NULL,
    CONSTRAINT employees_pkey PRIMARY KEY (id),
    CONSTRAINT department_fk FOREIGN KEY (department_id)
        REFERENCES public.departments (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT job_fk FOREIGN KEY (job_id)
        REFERENCES public.jobs (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT line_manager_fk FOREIGN KEY (line_manager_id)
        REFERENCES public.employees (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE public.employees
    OWNER to postgres;

CREATE INDEX fki_department_fk
    ON public.employees USING btree
    (department_id ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE INDEX fki_job_fk
    ON public.employees USING btree
    (job_id ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE INDEX fki_line_manager_fk
    ON public.employees USING btree
    (line_manager_id ASC NULLS LAST)
    TABLESPACE pg_default;

-- Comments creation
CREATE SEQUENCE public.comments_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.comments_id_seq
    OWNER TO postgres;

CREATE TABLE public.comments
(
    id integer NOT NULL DEFAULT nextval('comments_id_seq'::regclass),
    date_created date NOT NULL,
    author character varying(100) COLLATE pg_catalog."default" NOT NULL,
    content character varying(500) COLLATE pg_catalog."default" NOT NULL,
    employee_id integer NOT NULL,
    CONSTRAINT employee_fk FOREIGN KEY (employee_id)
        REFERENCES public.employees (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE public.comments
    OWNER to postgres;

CREATE INDEX fki_employee_fk
    ON public.comments USING btree
    (employee_id ASC NULLS LAST)
    TABLESPACE pg_default;