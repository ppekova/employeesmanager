-- Seed departments.
INSERT INTO public.departments(name) VALUES ('Sales');
INSERT INTO public.departments(name) VALUES ('IT Support');
INSERT INTO public.departments(name) VALUES ('Accounting');

-- Seed jobs.
INSERT INTO public.jobs(title) VALUES ('Sales Consultant');
INSERT INTO public.jobs(title) VALUES ('IT Expert');
INSERT INTO public.jobs(title) VALUES ('Accountant');

-- Seed employees.
INSERT INTO public.employees(
	name, family, address, salary_amount_type, salary_amount, job_id, department_id, joined_company_date)
	VALUES ('John', 'Peterson', '245 Green Str', 0, 1523.36, 1, 1, NOW());
INSERT INTO public.employees(
	name, family, address, salary_amount_type, salary_amount, job_id, department_id, line_manager_id, joined_company_date)
	VALUES ('Thelma', 'Wilson', '12 Blue Str', 0, 1000, 1, 1, 1, NOW());
INSERT INTO public.employees(
	name, family, address, salary_amount_type, salary_amount, job_id, department_id, joined_company_date)
	VALUES ('David', 'Johnson', '2342 Pink Str', 1, 16000.17, 2, 2, NOW());	
INSERT INTO public.employees(
	name, family, address, salary_amount_type, salary_amount, job_id, department_id, line_manager_id, joined_company_date)
	VALUES ('Steve', 'Simpson', '212 Yellow Str', 1, 10050, 2, 2, 3, NOW());	