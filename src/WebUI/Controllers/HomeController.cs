﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using EmployeesManager.Models;
using MediatR;
using System.Threading;
using Application.Employees.Models;

namespace EmployeesManager.Controllers
{
    public class HomeController : Controller
    {
        private IMediator _mediator;
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> Index(CancellationToken cancellationToken)
        {
            ViewBag.Jobs = await _mediator.Send(new Application.Queries.Jobs.GetJobsQuery(), cancellationToken);
            ViewBag.Departments = await _mediator.Send(new Application.Queries.Departments.GetDepartmentsQuery(), cancellationToken);

            var results = await _mediator.Send(new Application.Employees.Queries.GetEmployeesQuery());

            return View(results);
        }

        [HttpPost]
        public PartialViewResult Search([FromBody] SearchFilterViewModel searchFilter)
        {
            var searchQuery = new Application.Employees.Queries.GetEmployeesQuery()
            {
                SearchFilterViewModel = searchFilter
            };

            var res = _mediator.Send(searchQuery, CancellationToken.None).Result;

            return PartialView("_Employees", res);
        }

        public IActionResult Comments(int id)
        {
            return RedirectToAction("Index", "Comments", id);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
