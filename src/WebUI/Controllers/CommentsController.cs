﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Application.Comments.Commands;
using Application.Comments.Models;
using Application.Comments.Queries;
using EmployeesManager.Models;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace WebUI.Controllers
{
    public class CommentsController : Controller
    {
        private IMediator _mediator;
        private readonly ILogger<CommentsController> _logger;

        public CommentsController(ILogger<CommentsController> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        public async Task<IActionResult> Index(int id)
        {
            ViewBag.EmployeeID = id;
            var res = await _mediator.Send(new GetCommentsQuery() { EmployeeID = id }); 

            return View(res);
        }

        [HttpGet]
        public IActionResult Create(int id)
        {
            var viewModel = new CommentViewModel() { EmployeeID = id};
            return View(viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Create(CommentViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _mediator.Send(new CreateCommentCommand() { CreateCommentViewModel = viewModel });
            return RedirectToAction("Index", "Comments", new { id = viewModel.EmployeeID });
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var res = await _mediator.Send(new GetCommentQuery() { CommentID = id });
            return View(res);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(CommentViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _mediator.Send(new EditCommentCommand() { EditCommentViewModel = viewModel });
            return RedirectToAction("Index", "Comments", new { id = viewModel.EmployeeID });
        }

        public async Task<IActionResult> Delete(int id, int employeeId) 
        {
            await _mediator.Send(new DeleteCommentCommand() { CommentID = id });
            return RedirectToAction("Index", "Comments", new { id = employeeId });
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
