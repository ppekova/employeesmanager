﻿using Dapper;
using Infrastructure.Persistence;
using Microsoft.Extensions.Options;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Integration.Tests
{
    public class CleanUpDatabaseFixturesService
    {
        IOptions<DatabaseOptions> options;

        public CleanUpDatabaseFixturesService(IOptions<DatabaseOptions> options)
        {
            this.options = options;
        }

        public void TryCleanUp()
        {
            try
            {
                using var connection = new NpgsqlConnection(options.Value.DBHost);
                connection.Open();
                connection.Execute($"DROP DATABASE \"{options.Value.DBName}\" WITH (FORCE);");
            }
            catch(Exception ex)
            {
                Console.WriteLine("WARNING Wasn't able to clean up database fixtures.");
                Console.WriteLine(ex.Message);
            }
        }
    }
}
