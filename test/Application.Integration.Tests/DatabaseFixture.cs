using Application.Common.Interfaces;
using Infrastructure.Persistence;
using Infrastructure.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;

namespace Application.Integration.Tests
{
    public class DatabaseFixture : IDisposable
    {
        ServiceProvider serviceProvider;
        public IDatabaseAdapterService DatabaseAdapterService;

        public DatabaseFixture()
        {
            serviceProvider = ConfigureServices().BuildServiceProvider();
            var migrationService = serviceProvider.GetService<IDatabaseMigrationService>();
            migrationService.GetContextAndMigrate();

            DatabaseAdapterService = serviceProvider.GetService<IDatabaseAdapterService>();
        }

        private static ServiceCollection ConfigureServices()
        {
            var serviceCollection = new ServiceCollection();

            serviceCollection.Configure<DatabaseOptions>(options =>
            {
                options.DBHost = "User ID=postgres;Password=test;Host=localhost;Port=5432;Pooling=true;";
                options.DBName = Guid.NewGuid().ToString("N");
            });

            serviceCollection.AddTransient<IDatabaseMigrationService, DatabaseMigrationService>();
            serviceCollection.AddTransient<IDatabaseAdapterService, DatabaseAdapterService>();
            serviceCollection.AddTransient<CleanUpDatabaseFixturesService>();

            return serviceCollection;
        }

        public void Dispose()
        {
            var cleanUpService = serviceProvider.GetService<CleanUpDatabaseFixturesService>();
            cleanUpService.TryCleanUp();
        }
    }
}
