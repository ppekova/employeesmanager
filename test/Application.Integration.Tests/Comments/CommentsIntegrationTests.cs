﻿using Application.Comments.Commands;
using Application.Comments.Models;
using Application.Comments.Queries;
using AutoMapper;
using Shouldly;
using Solarity.Web.Service.Application.Common.Mappings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Application.Integration.Tests.Employees.Commands.CreateEmployee
{
    public class CommentsIntegrationTests : IDisposable
    {
        DatabaseFixture databaseFixture;
        public CommentsIntegrationTests()
        {
            this.databaseFixture = new DatabaseFixture();
        }

        public void Dispose()
        {
            this.databaseFixture.Dispose();
        }

        [Fact]
        public async Task Handle_Create_And_Get_Comment()
        {
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new EmployeesExplorerProfile());
            });
            var mapper = mockMapper.CreateMapper();

            var createCommand = new CreateCommentCommand()
            {
                CreateCommentViewModel = new CommentViewModel()
                {
                    Author = "Patrick Williams",
                    EmployeeID = 1,
                    Content = "Promoted."
                }
            };

            var createHandler = new CreateCommentCommand.CreateCommentCommandHandler(databaseFixture.DatabaseAdapterService, mapper);
            var createResult = await createHandler.Handle(createCommand, CancellationToken.None);

            var getCommand = new GetCommentQuery()
            {
                CommentID = createResult
            };

            var getHandler = new GetCommentQuery.GetCommentQueryHandler(databaseFixture.DatabaseAdapterService, mapper);
            var getResult = await getHandler.Handle(getCommand, CancellationToken.None);

            createResult.ShouldNotBe(0, "Result after insert is 0.");
            getResult.ID.ShouldBeGreaterThan(0, "ID value is 0.");
            getResult.Author.ShouldBe(createCommand.CreateCommentViewModel.Author, "Author is not with correct value.");
            getResult.Content.ShouldBe(createCommand.CreateCommentViewModel.Content, "Content is not with correct value.");
            getResult.DateCreated.ToString("yyyy-MM-dd HH:mm:ss").ShouldBe(createCommand.CreateCommentViewModel.DateCreated.ToString("yyyy-MM-dd HH:mm:ss"), "DateCreated is not with correct value.");
            getResult.EmployeeID.ShouldBe(createCommand.CreateCommentViewModel.EmployeeID, "DateCreated is not with correct value.");
        }
    }
}
